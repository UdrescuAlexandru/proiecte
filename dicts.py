#import pprint 
from pprint import pprint

personDictionary = {}
personDictionary['age'] = 18
personDictionary['name'] = 'andrei'
personDictionary['children'] = []
personDictionary['children'].append({'age' : 2,'name' : 'jim'})
personDictionary['children'].append({'age' : 2,'name' : 'ann'})

#pprint(personDictionary)
#print(type(personDictionary))

if 'n' in personDictionary:
     print(personDictionary['n'])
else: 
     print('no key "n" in dictionary')
     
for key in personDictionary:
     print(key)
     print(personDictionary[key])
     
for key, value in personDictionary.items():
    print(value)
     
     
if 'name' in personDictionary:     
     del personDictionary['name']  
     
for key, value in personDictionary.items():
    print(value)
    
    