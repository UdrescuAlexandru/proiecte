def hello(name, fromWhom = 'someone'):
    return 'hello ' + name + ' from ' + fromWhom

def testDynamic(paramSwitch):
    if paramSwitch == 'n':
        return 5
    else:
        return 'some text'

print(type(testDynamic('n')))

#print(hello('andrei', fromWhom = 'someone else'))