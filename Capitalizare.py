def Capital(word):
    listaExceptii = ['a', 'is', 'the']
    if word not in listaExceptii:
        return word.capitalize()
    return word
    
input = 'have a nice day'
input.capitalize()    

vect = input.split(" ") 
x = map(Capital, vect)
print " ".join(x)   